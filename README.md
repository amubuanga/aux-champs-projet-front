# Aux champs

Je ne sais pas vous, mais c’est je suis toujours en train de me
creuser la tête au moment d’aller au marché ou au super marché
pour ré-écrire ma liste de courses…

Vous arrivez à aller sur le lien ? 

https://auxchamps.mubuanga.me

Dans tous les cas, je vous explique ce que s'est !

Aux champs est une plateforme qui vous permet de créer et gérer vos listes de courses en ligne.
N'attendez, c'est gratuit et simple à prendre en main.

Créer votre liste de course, en présisant une boutique et ensuite ajoutez-y des articles, supprimer après achat et le tour est joué !

Dans la prochaine version, nous allons vous ajouter la possibilité de payer directement sur notre plateforme.

**Profitez de l'expérience aux champs!**

