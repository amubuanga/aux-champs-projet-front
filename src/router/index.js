import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/listing",
      name: "Listing",
      component: () =>
        import(/* webpackChunkName: "Listing" */ "../views/Listing")
    },
    {
      path: "/new",
      name: "AddListe",
      component: () =>
        import(/* webpackChunkName: "AddListe" */ "../views/AddListe")
    },
    {
      path: "/list/:slug/add",
      name: "AddItem",
      props: true,
      component: () =>
        import(/* webpackChunkName AddItem */ "../views/AddItem")
    },
    {
      path: "/list/:slug",
      name: "ListDetails",
      component: () =>
        import(/* webpackChunkName: "ListDetails" */ "../views/ListDetails"),
      children: [
        {
          path: "add",
          name: "AddItem",
          props: true,
          component: () =>
            import(/* webpackChunkName AddItem */ "../views/AddItem")
        }
      ],
      beforeEnter: (to, from, next) => {
        let lists;
        if (localStorage.getItem("lists")) {
          try {
            lists = JSON.parse(localStorage.getItem("lists"));
          } catch (e) {
            localStorage.removeItem("lists");
          }
        }
        const exists = lists.find(list => list.slug === to.params.slug);
        if (exists) {
          next();
        } else {
          next({ name: "NotFound" });
        }
      }
    }
  ]
});
